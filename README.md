# Fedora Robotics SIG Ansible Playbooks

A collection of playbooks for the Robotics SIG, each ansible role is hosted
in its own repository (see requirements.yml).


## License

[Apache-2.0](./LICENSE)
